<?php

/* TwigBundle:Exception:exception_full.html.twig */
class __TwigTemplate_02a23df4642af9fe75ecbeacff6057a510620764a2550d73881595b753455cc6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("TwigBundle::layout.html.twig", "TwigBundle:Exception:exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "TwigBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8d236978951f55dc9a865096f427d2af5ec44d571318f9fe395f120a2f675620 = $this->env->getExtension("native_profiler");
        $__internal_8d236978951f55dc9a865096f427d2af5ec44d571318f9fe395f120a2f675620->enter($__internal_8d236978951f55dc9a865096f427d2af5ec44d571318f9fe395f120a2f675620_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8d236978951f55dc9a865096f427d2af5ec44d571318f9fe395f120a2f675620->leave($__internal_8d236978951f55dc9a865096f427d2af5ec44d571318f9fe395f120a2f675620_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_232c4e0fa35e716622798ba518fc1473b8f0ceb3b478dd056ceaf4155ef7657c = $this->env->getExtension("native_profiler");
        $__internal_232c4e0fa35e716622798ba518fc1473b8f0ceb3b478dd056ceaf4155ef7657c->enter($__internal_232c4e0fa35e716622798ba518fc1473b8f0ceb3b478dd056ceaf4155ef7657c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_232c4e0fa35e716622798ba518fc1473b8f0ceb3b478dd056ceaf4155ef7657c->leave($__internal_232c4e0fa35e716622798ba518fc1473b8f0ceb3b478dd056ceaf4155ef7657c_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_47d52194fbd64a7510e63a188fef0be8acff67ec0f1aadf8d577d36dfc564073 = $this->env->getExtension("native_profiler");
        $__internal_47d52194fbd64a7510e63a188fef0be8acff67ec0f1aadf8d577d36dfc564073->enter($__internal_47d52194fbd64a7510e63a188fef0be8acff67ec0f1aadf8d577d36dfc564073_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_47d52194fbd64a7510e63a188fef0be8acff67ec0f1aadf8d577d36dfc564073->leave($__internal_47d52194fbd64a7510e63a188fef0be8acff67ec0f1aadf8d577d36dfc564073_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_733437f62db48717fb7d5601443b6c785ca6d11f7cdcbd2ba6bc9b241dbb856b = $this->env->getExtension("native_profiler");
        $__internal_733437f62db48717fb7d5601443b6c785ca6d11f7cdcbd2ba6bc9b241dbb856b->enter($__internal_733437f62db48717fb7d5601443b6c785ca6d11f7cdcbd2ba6bc9b241dbb856b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("TwigBundle:Exception:exception.html.twig", "TwigBundle:Exception:exception_full.html.twig", 12)->display($context);
        
        $__internal_733437f62db48717fb7d5601443b6c785ca6d11f7cdcbd2ba6bc9b241dbb856b->leave($__internal_733437f62db48717fb7d5601443b6c785ca6d11f7cdcbd2ba6bc9b241dbb856b_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
