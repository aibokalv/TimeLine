<?php

/* base.html.twig */
class __TwigTemplate_07effad5f0e9a94bcbd163b530af8b3ec8bb2b2d514b757d3afa386295c5be7b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_77b2f77cc06c3cfe951282f5987543756b77a85ab667eae4d3581b89f45a68d6 = $this->env->getExtension("native_profiler");
        $__internal_77b2f77cc06c3cfe951282f5987543756b77a85ab667eae4d3581b89f45a68d6->enter($__internal_77b2f77cc06c3cfe951282f5987543756b77a85ab667eae4d3581b89f45a68d6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "    </body>
</html>
";
        
        $__internal_77b2f77cc06c3cfe951282f5987543756b77a85ab667eae4d3581b89f45a68d6->leave($__internal_77b2f77cc06c3cfe951282f5987543756b77a85ab667eae4d3581b89f45a68d6_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_a58203247049d0bdb25618a5b114ec5be564b0649970958bd2e23cda1406424b = $this->env->getExtension("native_profiler");
        $__internal_a58203247049d0bdb25618a5b114ec5be564b0649970958bd2e23cda1406424b->enter($__internal_a58203247049d0bdb25618a5b114ec5be564b0649970958bd2e23cda1406424b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_a58203247049d0bdb25618a5b114ec5be564b0649970958bd2e23cda1406424b->leave($__internal_a58203247049d0bdb25618a5b114ec5be564b0649970958bd2e23cda1406424b_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_de22822a61c26dcaf989d79fabc39113f6e8840939342611a7f107fcbcee76c6 = $this->env->getExtension("native_profiler");
        $__internal_de22822a61c26dcaf989d79fabc39113f6e8840939342611a7f107fcbcee76c6->enter($__internal_de22822a61c26dcaf989d79fabc39113f6e8840939342611a7f107fcbcee76c6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_de22822a61c26dcaf989d79fabc39113f6e8840939342611a7f107fcbcee76c6->leave($__internal_de22822a61c26dcaf989d79fabc39113f6e8840939342611a7f107fcbcee76c6_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_210897b563212e28902ad19b3b0b1752ab39d0198c8da0ac717425bee24a128f = $this->env->getExtension("native_profiler");
        $__internal_210897b563212e28902ad19b3b0b1752ab39d0198c8da0ac717425bee24a128f->enter($__internal_210897b563212e28902ad19b3b0b1752ab39d0198c8da0ac717425bee24a128f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_210897b563212e28902ad19b3b0b1752ab39d0198c8da0ac717425bee24a128f->leave($__internal_210897b563212e28902ad19b3b0b1752ab39d0198c8da0ac717425bee24a128f_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_a12cb45c757dfe9d60ba3d186f82c73e67695ac3a0a4d90e0a56dbcf305210d6 = $this->env->getExtension("native_profiler");
        $__internal_a12cb45c757dfe9d60ba3d186f82c73e67695ac3a0a4d90e0a56dbcf305210d6->enter($__internal_a12cb45c757dfe9d60ba3d186f82c73e67695ac3a0a4d90e0a56dbcf305210d6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_a12cb45c757dfe9d60ba3d186f82c73e67695ac3a0a4d90e0a56dbcf305210d6->leave($__internal_a12cb45c757dfe9d60ba3d186f82c73e67695ac3a0a4d90e0a56dbcf305210d6_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 11,  82 => 10,  71 => 6,  59 => 5,  50 => 12,  47 => 11,  45 => 10,  38 => 7,  36 => 6,  32 => 5,  26 => 1,);
    }
}
