<?php

/* default/index.html.twig */
class __TwigTemplate_bdbc3115568ba5f48ab772c443b20c5f890e77b9ae4ea4acd529a255723c025a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "default/index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9dc7a74bb6ae1ac18c598843725f4de5478b590747089946a14a1abdcd934b0e = $this->env->getExtension("native_profiler");
        $__internal_9dc7a74bb6ae1ac18c598843725f4de5478b590747089946a14a1abdcd934b0e->enter($__internal_9dc7a74bb6ae1ac18c598843725f4de5478b590747089946a14a1abdcd934b0e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9dc7a74bb6ae1ac18c598843725f4de5478b590747089946a14a1abdcd934b0e->leave($__internal_9dc7a74bb6ae1ac18c598843725f4de5478b590747089946a14a1abdcd934b0e_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_db4a8dc24ec067be57dafd9513d21c6f09bb0c47744464151c6463bb4d4b6c98 = $this->env->getExtension("native_profiler");
        $__internal_db4a8dc24ec067be57dafd9513d21c6f09bb0c47744464151c6463bb4d4b6c98->enter($__internal_db4a8dc24ec067be57dafd9513d21c6f09bb0c47744464151c6463bb4d4b6c98_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    Hello world {limit}.
";
        
        $__internal_db4a8dc24ec067be57dafd9513d21c6f09bb0c47744464151c6463bb4d4b6c98->leave($__internal_db4a8dc24ec067be57dafd9513d21c6f09bb0c47744464151c6463bb4d4b6c98_prof);

    }

    public function getTemplateName()
    {
        return "default/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
