<?php

/* base.html.twig */
class __TwigTemplate_613ea64d6efe45fe9ca707cf3196d76677d691f6b418e56757555148c4510f48 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_04d81116504ac5d26e6faf021766e60819c481d42de783c9ec6d7b74cd3704de = $this->env->getExtension("native_profiler");
        $__internal_04d81116504ac5d26e6faf021766e60819c481d42de783c9ec6d7b74cd3704de->enter($__internal_04d81116504ac5d26e6faf021766e60819c481d42de783c9ec6d7b74cd3704de_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>

        ";
        // line 11
        $this->displayBlock('body', $context, $blocks);
        // line 12
        echo "            </body>
</html>
";
        
        $__internal_04d81116504ac5d26e6faf021766e60819c481d42de783c9ec6d7b74cd3704de->leave($__internal_04d81116504ac5d26e6faf021766e60819c481d42de783c9ec6d7b74cd3704de_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_2225236ab9e172983446791943a28c81768a8ed918fce0e4dc9061beae93dc4d = $this->env->getExtension("native_profiler");
        $__internal_2225236ab9e172983446791943a28c81768a8ed918fce0e4dc9061beae93dc4d->enter($__internal_2225236ab9e172983446791943a28c81768a8ed918fce0e4dc9061beae93dc4d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_2225236ab9e172983446791943a28c81768a8ed918fce0e4dc9061beae93dc4d->leave($__internal_2225236ab9e172983446791943a28c81768a8ed918fce0e4dc9061beae93dc4d_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_5ed7c69b5346067c61b6dc34f0a2ecf8c5a66665db79fe7a0d234d472273f540 = $this->env->getExtension("native_profiler");
        $__internal_5ed7c69b5346067c61b6dc34f0a2ecf8c5a66665db79fe7a0d234d472273f540->enter($__internal_5ed7c69b5346067c61b6dc34f0a2ecf8c5a66665db79fe7a0d234d472273f540_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_5ed7c69b5346067c61b6dc34f0a2ecf8c5a66665db79fe7a0d234d472273f540->leave($__internal_5ed7c69b5346067c61b6dc34f0a2ecf8c5a66665db79fe7a0d234d472273f540_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_f827f68cf82c43c2f7db518e4ddf32e055790f7ff4becbdf6a47c2b641ffdd73 = $this->env->getExtension("native_profiler");
        $__internal_f827f68cf82c43c2f7db518e4ddf32e055790f7ff4becbdf6a47c2b641ffdd73->enter($__internal_f827f68cf82c43c2f7db518e4ddf32e055790f7ff4becbdf6a47c2b641ffdd73_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_f827f68cf82c43c2f7db518e4ddf32e055790f7ff4becbdf6a47c2b641ffdd73->leave($__internal_f827f68cf82c43c2f7db518e4ddf32e055790f7ff4becbdf6a47c2b641ffdd73_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  79 => 11,  68 => 6,  56 => 5,  47 => 12,  45 => 11,  37 => 7,  35 => 6,  31 => 5,  25 => 1,);
    }
}
