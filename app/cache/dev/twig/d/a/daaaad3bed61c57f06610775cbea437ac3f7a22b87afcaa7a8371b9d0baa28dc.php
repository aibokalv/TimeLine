<?php

/* base.html.twig */
class __TwigTemplate_daaaad3bed61c57f06610775cbea437ac3f7a22b87afcaa7a8371b9d0baa28dc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9129abbaaa7acc10107c35f9bb031c1de204ebffbdc019e6a451394b5d84be16 = $this->env->getExtension("native_profiler");
        $__internal_9129abbaaa7acc10107c35f9bb031c1de204ebffbdc019e6a451394b5d84be16->enter($__internal_9129abbaaa7acc10107c35f9bb031c1de204ebffbdc019e6a451394b5d84be16_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>

        ";
        // line 11
        $this->displayBlock('body', $context, $blocks);
        // line 12
        echo "            </body>
</html>
";
        
        $__internal_9129abbaaa7acc10107c35f9bb031c1de204ebffbdc019e6a451394b5d84be16->leave($__internal_9129abbaaa7acc10107c35f9bb031c1de204ebffbdc019e6a451394b5d84be16_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_e4d14d58195bc897feaa482be4dd83dc0283102c42037064f51a6d2e70759257 = $this->env->getExtension("native_profiler");
        $__internal_e4d14d58195bc897feaa482be4dd83dc0283102c42037064f51a6d2e70759257->enter($__internal_e4d14d58195bc897feaa482be4dd83dc0283102c42037064f51a6d2e70759257_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_e4d14d58195bc897feaa482be4dd83dc0283102c42037064f51a6d2e70759257->leave($__internal_e4d14d58195bc897feaa482be4dd83dc0283102c42037064f51a6d2e70759257_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_327a6f21a7812fe6b63c37ce17fa1a75c6fee28647f29aade0d575d002d4bc9e = $this->env->getExtension("native_profiler");
        $__internal_327a6f21a7812fe6b63c37ce17fa1a75c6fee28647f29aade0d575d002d4bc9e->enter($__internal_327a6f21a7812fe6b63c37ce17fa1a75c6fee28647f29aade0d575d002d4bc9e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_327a6f21a7812fe6b63c37ce17fa1a75c6fee28647f29aade0d575d002d4bc9e->leave($__internal_327a6f21a7812fe6b63c37ce17fa1a75c6fee28647f29aade0d575d002d4bc9e_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_8a6bdc86d7acb5ebf892a770ad0f82386987e4d197fe6e56859317d6f5769e04 = $this->env->getExtension("native_profiler");
        $__internal_8a6bdc86d7acb5ebf892a770ad0f82386987e4d197fe6e56859317d6f5769e04->enter($__internal_8a6bdc86d7acb5ebf892a770ad0f82386987e4d197fe6e56859317d6f5769e04_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_8a6bdc86d7acb5ebf892a770ad0f82386987e4d197fe6e56859317d6f5769e04->leave($__internal_8a6bdc86d7acb5ebf892a770ad0f82386987e4d197fe6e56859317d6f5769e04_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  79 => 11,  68 => 6,  56 => 5,  47 => 12,  45 => 11,  37 => 7,  35 => 6,  31 => 5,  25 => 1,);
    }
}
