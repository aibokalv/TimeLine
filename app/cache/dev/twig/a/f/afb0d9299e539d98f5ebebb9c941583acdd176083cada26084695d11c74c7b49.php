<?php

/* base.html.twig */
class __TwigTemplate_afb0d9299e539d98f5ebebb9c941583acdd176083cada26084695d11c74c7b49 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_06097ab2f0670b7265563a9229158f46ea3ea818edf9986c22d217acd17825f1 = $this->env->getExtension("native_profiler");
        $__internal_06097ab2f0670b7265563a9229158f46ea3ea818edf9986c22d217acd17825f1->enter($__internal_06097ab2f0670b7265563a9229158f46ea3ea818edf9986c22d217acd17825f1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>

        ";
        // line 11
        $this->displayBlock('body', $context, $blocks);
        // line 12
        echo "            </body>
</html>
";
        
        $__internal_06097ab2f0670b7265563a9229158f46ea3ea818edf9986c22d217acd17825f1->leave($__internal_06097ab2f0670b7265563a9229158f46ea3ea818edf9986c22d217acd17825f1_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_b530454559c28418fb74b1b568daf780c0ba02b1fa87e608558781286df40cd2 = $this->env->getExtension("native_profiler");
        $__internal_b530454559c28418fb74b1b568daf780c0ba02b1fa87e608558781286df40cd2->enter($__internal_b530454559c28418fb74b1b568daf780c0ba02b1fa87e608558781286df40cd2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_b530454559c28418fb74b1b568daf780c0ba02b1fa87e608558781286df40cd2->leave($__internal_b530454559c28418fb74b1b568daf780c0ba02b1fa87e608558781286df40cd2_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_739cac71a7b3888c5dd36487cc8b375ba061b08fb4dfcb8a42dfd81c86069001 = $this->env->getExtension("native_profiler");
        $__internal_739cac71a7b3888c5dd36487cc8b375ba061b08fb4dfcb8a42dfd81c86069001->enter($__internal_739cac71a7b3888c5dd36487cc8b375ba061b08fb4dfcb8a42dfd81c86069001_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_739cac71a7b3888c5dd36487cc8b375ba061b08fb4dfcb8a42dfd81c86069001->leave($__internal_739cac71a7b3888c5dd36487cc8b375ba061b08fb4dfcb8a42dfd81c86069001_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_f2af949ec2f1b3a0d6ee670b80cb062630079baf692cfeeff905ba0d9aad1286 = $this->env->getExtension("native_profiler");
        $__internal_f2af949ec2f1b3a0d6ee670b80cb062630079baf692cfeeff905ba0d9aad1286->enter($__internal_f2af949ec2f1b3a0d6ee670b80cb062630079baf692cfeeff905ba0d9aad1286_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_f2af949ec2f1b3a0d6ee670b80cb062630079baf692cfeeff905ba0d9aad1286->leave($__internal_f2af949ec2f1b3a0d6ee670b80cb062630079baf692cfeeff905ba0d9aad1286_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  79 => 11,  68 => 6,  56 => 5,  47 => 12,  45 => 11,  37 => 7,  35 => 6,  31 => 5,  25 => 1,);
    }
}
