<?php

/* AppBundle:News:index.html.twig */
class __TwigTemplate_e631805ef108d0a2141ef9df7074e840ce1e873a9e7270de5e673152590423d8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "AppBundle:News:index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_526ef0f77e3b8f183ccabaf7470b6fedf1cc712b20a8cffe7edf2b22e3a7fb54 = $this->env->getExtension("native_profiler");
        $__internal_526ef0f77e3b8f183ccabaf7470b6fedf1cc712b20a8cffe7edf2b22e3a7fb54->enter($__internal_526ef0f77e3b8f183ccabaf7470b6fedf1cc712b20a8cffe7edf2b22e3a7fb54_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:News:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_526ef0f77e3b8f183ccabaf7470b6fedf1cc712b20a8cffe7edf2b22e3a7fb54->leave($__internal_526ef0f77e3b8f183ccabaf7470b6fedf1cc712b20a8cffe7edf2b22e3a7fb54_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_4896039c3d0d7313891ad234c283e1aa150896a783800d56d4ac34f6daf77364 = $this->env->getExtension("native_profiler");
        $__internal_4896039c3d0d7313891ad234c283e1aa150896a783800d56d4ac34f6daf77364->enter($__internal_4896039c3d0d7313891ad234c283e1aa150896a783800d56d4ac34f6daf77364_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<h1>News list</h1>

    <table class=\"records_list\">
        <thead>
            <tr>
                <th>Id</th>
                <th>Title</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 15
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 16
            echo "            <tr>
                <td><a href=\"";
            // line 17
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("news_show", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
            echo "</a></td>
                <td>";
            // line 18
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "title", array()), "html", null, true);
            echo "</td>
                <td>
                <ul>
                    <li>
                        <a href=\"";
            // line 22
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("news_show", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
            echo "\">show</a>
                    </li>
                </ul>
                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 28
        echo "        </tbody>
    </table>

    ";
        
        $__internal_4896039c3d0d7313891ad234c283e1aa150896a783800d56d4ac34f6daf77364->leave($__internal_4896039c3d0d7313891ad234c283e1aa150896a783800d56d4ac34f6daf77364_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:News:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 28,  73 => 22,  66 => 18,  60 => 17,  57 => 16,  53 => 15,  40 => 4,  34 => 3,  11 => 1,);
    }
}
