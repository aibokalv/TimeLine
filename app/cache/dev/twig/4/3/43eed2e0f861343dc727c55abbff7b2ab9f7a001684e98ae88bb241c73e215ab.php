<?php

/* AppBundle:News:show.html.twig */
class __TwigTemplate_43eed2e0f861343dc727c55abbff7b2ab9f7a001684e98ae88bb241c73e215ab extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "AppBundle:News:show.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_19f32e0f97326eb1e85ff91d9b5a257199fecd1200385087010db86c8ded3ac9 = $this->env->getExtension("native_profiler");
        $__internal_19f32e0f97326eb1e85ff91d9b5a257199fecd1200385087010db86c8ded3ac9->enter($__internal_19f32e0f97326eb1e85ff91d9b5a257199fecd1200385087010db86c8ded3ac9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:News:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_19f32e0f97326eb1e85ff91d9b5a257199fecd1200385087010db86c8ded3ac9->leave($__internal_19f32e0f97326eb1e85ff91d9b5a257199fecd1200385087010db86c8ded3ac9_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_befdc4cebcd2c1be478458be7416f1202ced76d93308e4471a42b64e458616fc = $this->env->getExtension("native_profiler");
        $__internal_befdc4cebcd2c1be478458be7416f1202ced76d93308e4471a42b64e458616fc->enter($__internal_befdc4cebcd2c1be478458be7416f1202ced76d93308e4471a42b64e458616fc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<h1>News</h1>

    <table class=\"record_properties\">
        <tbody>
            <tr>
                <th>Id</th>
                <td>";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Title</th>
                <td>";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "title", array()), "html", null, true);
        echo "</td>
            </tr>
        </tbody>
    </table>

        <ul class=\"record_actions\">
    <li>
        <a href=\"";
        // line 21
        echo $this->env->getExtension('routing')->getPath("news");
        echo "\">
            Back to the list
        </a>
    </li>
</ul>
";
        
        $__internal_befdc4cebcd2c1be478458be7416f1202ced76d93308e4471a42b64e458616fc->leave($__internal_befdc4cebcd2c1be478458be7416f1202ced76d93308e4471a42b64e458616fc_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:News:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  65 => 21,  55 => 14,  48 => 10,  40 => 4,  34 => 3,  11 => 1,);
    }
}
