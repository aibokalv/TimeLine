<?php

/* TwigBundle:Exception:exception_full.html.twig */
class __TwigTemplate_4499213609c77f04f597acccd5ac51dc1fe82d1bb87d01e7252a10521fff4058 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("TwigBundle::layout.html.twig", "TwigBundle:Exception:exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "TwigBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_55b8a251d1f6ffab369f8dd6319066971baac228097dd9f2d3b54bc6e7599895 = $this->env->getExtension("native_profiler");
        $__internal_55b8a251d1f6ffab369f8dd6319066971baac228097dd9f2d3b54bc6e7599895->enter($__internal_55b8a251d1f6ffab369f8dd6319066971baac228097dd9f2d3b54bc6e7599895_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_55b8a251d1f6ffab369f8dd6319066971baac228097dd9f2d3b54bc6e7599895->leave($__internal_55b8a251d1f6ffab369f8dd6319066971baac228097dd9f2d3b54bc6e7599895_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_be985e9ee83e5fa56371792d64d9c7c319684435d0c06d5dc573b090920e4ff1 = $this->env->getExtension("native_profiler");
        $__internal_be985e9ee83e5fa56371792d64d9c7c319684435d0c06d5dc573b090920e4ff1->enter($__internal_be985e9ee83e5fa56371792d64d9c7c319684435d0c06d5dc573b090920e4ff1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_be985e9ee83e5fa56371792d64d9c7c319684435d0c06d5dc573b090920e4ff1->leave($__internal_be985e9ee83e5fa56371792d64d9c7c319684435d0c06d5dc573b090920e4ff1_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_c279b4ae7919a9370f8a4e3c661117485594751eb26b4eca253873da1a0d670a = $this->env->getExtension("native_profiler");
        $__internal_c279b4ae7919a9370f8a4e3c661117485594751eb26b4eca253873da1a0d670a->enter($__internal_c279b4ae7919a9370f8a4e3c661117485594751eb26b4eca253873da1a0d670a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_c279b4ae7919a9370f8a4e3c661117485594751eb26b4eca253873da1a0d670a->leave($__internal_c279b4ae7919a9370f8a4e3c661117485594751eb26b4eca253873da1a0d670a_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_c1c86a3604b77ee64d0174186777b5d044b5714dae481223c0928fa4aed71070 = $this->env->getExtension("native_profiler");
        $__internal_c1c86a3604b77ee64d0174186777b5d044b5714dae481223c0928fa4aed71070->enter($__internal_c1c86a3604b77ee64d0174186777b5d044b5714dae481223c0928fa4aed71070_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("TwigBundle:Exception:exception.html.twig", "TwigBundle:Exception:exception_full.html.twig", 12)->display($context);
        
        $__internal_c1c86a3604b77ee64d0174186777b5d044b5714dae481223c0928fa4aed71070->leave($__internal_c1c86a3604b77ee64d0174186777b5d044b5714dae481223c0928fa4aed71070_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
