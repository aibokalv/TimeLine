<?php

/* ::base.html.twig */
class __TwigTemplate_29bc51aa1eec5c9f15717f416a3cf1214e62aa1a2ee9faf95de255a01c75d244 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_18a18e14b34f4b4bbf0809b479b71f17a69f900eae9b16c43cc2a262321b7a4e = $this->env->getExtension("native_profiler");
        $__internal_18a18e14b34f4b4bbf0809b479b71f17a69f900eae9b16c43cc2a262321b7a4e->enter($__internal_18a18e14b34f4b4bbf0809b479b71f17a69f900eae9b16c43cc2a262321b7a4e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>

        ";
        // line 11
        $this->displayBlock('body', $context, $blocks);
        // line 12
        echo "            </body>
</html>
";
        
        $__internal_18a18e14b34f4b4bbf0809b479b71f17a69f900eae9b16c43cc2a262321b7a4e->leave($__internal_18a18e14b34f4b4bbf0809b479b71f17a69f900eae9b16c43cc2a262321b7a4e_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_9e35db843c24535ddf0f0e31c19ad456e895ed596859ab8590da9162819ef74f = $this->env->getExtension("native_profiler");
        $__internal_9e35db843c24535ddf0f0e31c19ad456e895ed596859ab8590da9162819ef74f->enter($__internal_9e35db843c24535ddf0f0e31c19ad456e895ed596859ab8590da9162819ef74f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_9e35db843c24535ddf0f0e31c19ad456e895ed596859ab8590da9162819ef74f->leave($__internal_9e35db843c24535ddf0f0e31c19ad456e895ed596859ab8590da9162819ef74f_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_7175e007610530b1bd7a01ab2bc025a7ba2c3f47eac3818d979f79f72555f475 = $this->env->getExtension("native_profiler");
        $__internal_7175e007610530b1bd7a01ab2bc025a7ba2c3f47eac3818d979f79f72555f475->enter($__internal_7175e007610530b1bd7a01ab2bc025a7ba2c3f47eac3818d979f79f72555f475_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_7175e007610530b1bd7a01ab2bc025a7ba2c3f47eac3818d979f79f72555f475->leave($__internal_7175e007610530b1bd7a01ab2bc025a7ba2c3f47eac3818d979f79f72555f475_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_7a770b4454f70d7ca4ffd9c10f4ca8de5ca97facd27f567724b3f2301c232185 = $this->env->getExtension("native_profiler");
        $__internal_7a770b4454f70d7ca4ffd9c10f4ca8de5ca97facd27f567724b3f2301c232185->enter($__internal_7a770b4454f70d7ca4ffd9c10f4ca8de5ca97facd27f567724b3f2301c232185_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_7a770b4454f70d7ca4ffd9c10f4ca8de5ca97facd27f567724b3f2301c232185->leave($__internal_7a770b4454f70d7ca4ffd9c10f4ca8de5ca97facd27f567724b3f2301c232185_prof);

    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  79 => 11,  68 => 6,  56 => 5,  47 => 12,  45 => 11,  37 => 7,  35 => 6,  31 => 5,  25 => 1,);
    }
}
