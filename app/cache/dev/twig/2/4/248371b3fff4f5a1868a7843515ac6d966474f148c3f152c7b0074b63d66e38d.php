<?php

/* default/index.html.twig */
class __TwigTemplate_248371b3fff4f5a1868a7843515ac6d966474f148c3f152c7b0074b63d66e38d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "default/index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_48377f5bca787f1f41dc23addfb4afdd32b5e1041831e27bf99d685edef8a787 = $this->env->getExtension("native_profiler");
        $__internal_48377f5bca787f1f41dc23addfb4afdd32b5e1041831e27bf99d685edef8a787->enter($__internal_48377f5bca787f1f41dc23addfb4afdd32b5e1041831e27bf99d685edef8a787_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_48377f5bca787f1f41dc23addfb4afdd32b5e1041831e27bf99d685edef8a787->leave($__internal_48377f5bca787f1f41dc23addfb4afdd32b5e1041831e27bf99d685edef8a787_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_986f9a57d67d480e1a5bff20565ba70a7ce7d1503cb314882d3d4ba328cef5f3 = $this->env->getExtension("native_profiler");
        $__internal_986f9a57d67d480e1a5bff20565ba70a7ce7d1503cb314882d3d4ba328cef5f3->enter($__internal_986f9a57d67d480e1a5bff20565ba70a7ce7d1503cb314882d3d4ba328cef5f3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    Hello world .Homepage.
";
        
        $__internal_986f9a57d67d480e1a5bff20565ba70a7ce7d1503cb314882d3d4ba328cef5f3->leave($__internal_986f9a57d67d480e1a5bff20565ba70a7ce7d1503cb314882d3d4ba328cef5f3_prof);

    }

    public function getTemplateName()
    {
        return "default/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
