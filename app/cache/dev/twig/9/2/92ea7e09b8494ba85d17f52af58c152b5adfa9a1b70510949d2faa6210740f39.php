<?php

/* TwigBundle:Exception:exception_full.html.twig */
class __TwigTemplate_92ea7e09b8494ba85d17f52af58c152b5adfa9a1b70510949d2faa6210740f39 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("TwigBundle::layout.html.twig", "TwigBundle:Exception:exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "TwigBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a571910520bacef30bbceec39c79a482d3f3cda412eab4b46f70689227f05775 = $this->env->getExtension("native_profiler");
        $__internal_a571910520bacef30bbceec39c79a482d3f3cda412eab4b46f70689227f05775->enter($__internal_a571910520bacef30bbceec39c79a482d3f3cda412eab4b46f70689227f05775_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a571910520bacef30bbceec39c79a482d3f3cda412eab4b46f70689227f05775->leave($__internal_a571910520bacef30bbceec39c79a482d3f3cda412eab4b46f70689227f05775_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_e89aa7897064024e7c6eb2cf37fc0b0a369ecfdddbbdd94c5a72eeed722b5f26 = $this->env->getExtension("native_profiler");
        $__internal_e89aa7897064024e7c6eb2cf37fc0b0a369ecfdddbbdd94c5a72eeed722b5f26->enter($__internal_e89aa7897064024e7c6eb2cf37fc0b0a369ecfdddbbdd94c5a72eeed722b5f26_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_e89aa7897064024e7c6eb2cf37fc0b0a369ecfdddbbdd94c5a72eeed722b5f26->leave($__internal_e89aa7897064024e7c6eb2cf37fc0b0a369ecfdddbbdd94c5a72eeed722b5f26_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_b2250a9b1ec65f1d64a7d665c7595884dbea3800c93b87848f25e88768f91fab = $this->env->getExtension("native_profiler");
        $__internal_b2250a9b1ec65f1d64a7d665c7595884dbea3800c93b87848f25e88768f91fab->enter($__internal_b2250a9b1ec65f1d64a7d665c7595884dbea3800c93b87848f25e88768f91fab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_b2250a9b1ec65f1d64a7d665c7595884dbea3800c93b87848f25e88768f91fab->leave($__internal_b2250a9b1ec65f1d64a7d665c7595884dbea3800c93b87848f25e88768f91fab_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_f4c05b94224ab24ae8333f045ad819e10375ec8cf65678d01e892d4306309b63 = $this->env->getExtension("native_profiler");
        $__internal_f4c05b94224ab24ae8333f045ad819e10375ec8cf65678d01e892d4306309b63->enter($__internal_f4c05b94224ab24ae8333f045ad819e10375ec8cf65678d01e892d4306309b63_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("TwigBundle:Exception:exception.html.twig", "TwigBundle:Exception:exception_full.html.twig", 12)->display($context);
        
        $__internal_f4c05b94224ab24ae8333f045ad819e10375ec8cf65678d01e892d4306309b63->leave($__internal_f4c05b94224ab24ae8333f045ad819e10375ec8cf65678d01e892d4306309b63_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
