<?php

/* HelloBundle:Default:index.html.twig */
class __TwigTemplate_13148ad731459f534c44a8788f3f5017f8080745dc10a50394ed3db73f1bd4ed extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e21aea5abeb7ac5784466eb1803bfd5ee286aa1c1f6eb3badfc1c21d80d3e11b = $this->env->getExtension("native_profiler");
        $__internal_e21aea5abeb7ac5784466eb1803bfd5ee286aa1c1f6eb3badfc1c21d80d3e11b->enter($__internal_e21aea5abeb7ac5784466eb1803bfd5ee286aa1c1f6eb3badfc1c21d80d3e11b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "HelloBundle:Default:index.html.twig"));

        // line 1
        echo "Hello ";
        echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : $this->getContext($context, "name")), "html", null, true);
        echo "!
";
        
        $__internal_e21aea5abeb7ac5784466eb1803bfd5ee286aa1c1f6eb3badfc1c21d80d3e11b->leave($__internal_e21aea5abeb7ac5784466eb1803bfd5ee286aa1c1f6eb3badfc1c21d80d3e11b_prof);

    }

    public function getTemplateName()
    {
        return "HelloBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
