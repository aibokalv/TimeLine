<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        return $this->render('default/index.html.twig');
    }
     /**
     * @Route("/random/{limit}", name="random")
     */
    public function randomAction($limit)
    {
        return $this->render('default/index.html.twig',array('limit'=>$limit));
    }

}
